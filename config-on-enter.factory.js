(function () {
    'use strict';

    function onEnterConfig () {

        return {
            /* Prerender status */
            loadingContent: function () {

            }
        }
    }

    onEnterConfig.$inject = [];

    angular
        .module('bravoureAngularApp')
        .factory('onEnterConfig', onEnterConfig);

})();
